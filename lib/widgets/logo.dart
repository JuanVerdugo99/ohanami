import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  const Logo({
    Key? key,
    required this.titulo,
  }) : super(key: key);

  final String titulo;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 230,
        margin: EdgeInsets.only(top: 25),
        child: Column(
          children: [
            Image(image: AssetImage('assets/logo_ohanami.png')),
            Text(this.titulo, style: TextStyle(fontSize: 30),)
          ],
        ),
      ),
    );
  }
}