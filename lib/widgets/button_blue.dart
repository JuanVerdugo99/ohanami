import 'package:flutter/material.dart';

class ButtonBlue extends StatelessWidget {
  const ButtonBlue({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  final String text;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 2,
      highlightElevation: 5,
      padding: EdgeInsets.only(top: 10, bottom: 10),
      color: Colors.blue,
      shape: StadiumBorder(),
      onPressed: () {
        this.onPressed();
      },
      child: Container(
        width: double.infinity,
        child: Center(child: Text(this.text, style: TextStyle(color: Colors.white, fontSize: 18),)),
      )
    );
  }
}
