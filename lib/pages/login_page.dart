import 'package:flutter/material.dart';
import 'package:ohanami/widgets/button_blue.dart';
import 'package:provider/provider.dart';

import 'package:ohanami/services/auth_service.dart';
import 'package:ohanami/widgets/custom_input.dart';
import 'package:ohanami/widgets/labels.dart';
import 'package:ohanami/widgets/logo.dart';
import 'package:ohanami/helpers/show_alert.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Logo(titulo: 'Login',),
                _Form(),
                Labels(ruta: 'register', titulo: '¿No tienes cuenta?', subtitulo: 'Crea una ahora!')
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Form extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  _Form({Key? key}) : super(key: key);

  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {

  final emailCtrl = TextEditingController();
  final passCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {

    final authService = Provider.of<AuthService>(context);

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        children: [
          CustomInput(
            icon: Icons.mail_outline,
            placeholder: 'Correo',
            keyboardType: TextInputType.emailAddress,
            textController: emailCtrl,
            isPassword: false,
          ),
          CustomInput(
            icon: Icons.lock_outline,
            placeholder: 'Contraseña',
            keyboardType: TextInputType.emailAddress,
            textController: passCtrl,
            isPassword: true,
          ),
          ButtonBlue(
            text: 'Ingrese', 
            onPressed: () async {
              FocusScope.of(context).unfocus();
              final loginOk = await authService.login(emailCtrl.text.trim(), passCtrl.text.trim());
              
              if(loginOk) {
                Navigator.pushReplacementNamed(context, 'users');
              } else {
                showAlert(context, 'Login Incorrecto', 'Vuelva a introducir sus datos ');
              }
            }
          )
        ],
      ),
    );
  }
}