import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:ohanami/global/environment.dart';
import 'package:ohanami/models/users.dart';
import 'package:ohanami/pages/partida_detalle_page.dart';
import 'package:ohanami/services/auth_service.dart';

class PartidasPage extends StatefulWidget {
  PartidasPage({
    Key? key,
    required this.user,
  }) : super(key: key);

  final UserDb user;
  @override
  _PartidasPageState createState() => _PartidasPageState();
}

class _PartidasPageState extends State<PartidasPage> {

  RefreshController _refreshController = RefreshController(initialRefresh: false);
  late Map data;
  List usersData = [];

  getUsers() async {
    http.Response response = await http.get(Uri.parse('${Environment.apiUrl}/partida/${widget.user.uid}'));
    data = json.decode(response.body);
    
    setState(() {
      usersData = data['partidas'];
    });
  }

  @override
  void initState() {
    super.initState();
    getUsers();
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    return SmartRefresher(
        controller: _refreshController, 
        enablePullDown: true,
        onRefresh: _loadUsers,
        // header: WaterDropHeader(
        //   complete: Icon( Icons.check, color: Colors.blue[400]),
        // ),
        child: ListView.builder(
          itemCount: usersData.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                    // leading: Icon(Icons.videogame_asset_rounded),
                    title: Text("Partida ${usersData.length - index}"),
                    subtitle: Row(
                      children: [
                        Text("Fecha: ${usersData[index]["fecha"].toString().substring(0,10)}"),
                        // mostrarJugadores(cantidad: usersData.length - 1)
                        // Text("Fecha: ${usersData[index]["fecha"].toString().substring(0,10)}\nJugadores:\n  ${usersData[index]["jugadores"][0]["jugador"]} = ${usersData[index]["jugadores"][0]["puntuacion"]}\n  ${usersData[index]["jugadores"][1]["jugador"]} = ${usersData[index]["jugadores"][1]["puntuacion"]}\n  ${usersData[index]["jugadores"][1]["jugador"]} = ${usersData[index]["jugadores"][2] != null ? usersData[index]["jugadores"][2]["puntuacion"] : ''}\nPuntuaciones: ${usersData[index]["fecha"].toString().substring(0,10)}"),
                      ],
                      // ${usersData[index]["jugadores"][0]}
                    ),
                    onTap: () {
                     Navigator.push(context, 
                                    MaterialPageRoute(builder: (context) => PartidaDetallePage(userId: usersData[index]["_id"])));
                    },
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextButton(
                        child: const Text('Eliminar'),
                        onPressed: () async {
                          await authService.borrarPartida(usersData[index]["_id"]);
                          Navigator.pushReplacementNamed(context, 'users');
                        },
                      ),
                      const SizedBox(width: 8),
                     
                    ],
                  ),
                ]
              ) 
            );
          },
        ),
      );
  }

   _loadUsers() async {
    getUsers();
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.refreshCompleted();
  }

}

class mostrarJugadores extends StatefulWidget {
  const mostrarJugadores({
    Key? key,
    required this.cantidad,
  }) : super(key: key);
    final int cantidad;
    @override
    _mostrarJugadoresState createState() => _mostrarJugadoresState();
  }
 
  class _mostrarJugadoresState extends State<mostrarJugadores> {
    @override
    Widget build(BuildContext context) {
      if (widget.cantidad == 0) {
        return Text('Hola');
      } else if (widget.cantidad == 1) {
        return Text('Hola');
      } else if (widget.cantidad == 2) {
        return Text('Hola');
      } else if (widget.cantidad == 3) {
        return Text('Hola');
      }
      return Container(
        
      );
    }
  }
