import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ohanami/global/environment.dart';
import 'package:ohanami/models/partidas.dart';
import 'package:ohanami/pages/partidas_page.dart';
import 'package:ohanami/pages/stats_page.dart';
import 'package:ohanami/services/auth_service.dart';
import 'package:provider/provider.dart';

import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:http/http.dart' as http;

import 'package:ohanami/models/users.dart';

import 'create_page.dart';

class UsersPage extends StatefulWidget {
  const UsersPage({ Key? key }) : super(key: key);

  @override
  State<UsersPage> createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {

  int _actualPage = 0;

  

  getPage(index, user) {
    if (index == 0) {
      return PartidasPage(user: user);
    } else if(index == 1) {
      return CreatePage();
    } else if (index == 2) {
      return StatsPage();
    }
  }

  
  @override
  Widget build(BuildContext context) {

    final authService = Provider.of<AuthService>(context);
    final user = authService.user;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Bienvenido ${user.name}", style: TextStyle(color: Colors.black54)),
        elevation: 5,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon( Icons.exit_to_app, color: Colors.black54,),
          onPressed: () {
            Navigator.pushReplacementNamed(context, 'login');
            AuthService.deleteToken();
          },
        ),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 10),
            // child: Icon( Icons.check_circle, color: Colors.blue[400],),
            // child: Icon( Icons.add_circle, color: Colors.blue[400],),
          )
        ],
      ),
      // body: _pages[_actualPage],
      body: getPage(_actualPage, user),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black,
              blurRadius: 5,
            ),
          ],
        ),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            setState(() {
              _actualPage = index;
            });
          },
          currentIndex: _actualPage,
            items: [
              BottomNavigationBarItem(
                icon: new Icon(Icons.home, size: 40),
                title: new Text("Inicio"),
              ),
                // BottomNavigationBarItem(
                //   icon: new Icon(Icons.insert_chart_rounded, size: 40),
                //   title: new Text("Estadísticas"),
                // ),
              BottomNavigationBarItem(
                icon: new Icon(Icons.add, size: 40),
                title: new Text("Crear Partida"),
              )
            ],
          ),
      ),
    );
  }

  // ListView _listViewUsers() {
  //   return ListView.separated(
  //     physics: BouncingScrollPhysics(),
  //     itemBuilder: (_, i) => _userListTile(usersData[i]), 
  //     separatorBuilder: (_, i) => Divider(), 
  //     itemCount: usersData == null ? 0 : usersData.length);
  // }

  // ListTile _userListTile(Partida users) {
  //   return ListTile(
  //       title: Text(users.nombre),
  //       subtitle: Text(users.fecha.toString()),
  //       leading: CircleAvatar(
  //         child: Text(users.nombre.substring(0,2)),
  //         backgroundColor: Colors.blue[100],
  //       ),
  //       trailing: Container(
  //         width: 10,
  //         height: 10,
  //         decoration: BoxDecoration(
  //           borderRadius: BorderRadius.circular(100)
  //         )
  //       ),
  //     );
  // }
  
}
