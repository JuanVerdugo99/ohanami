import 'package:flutter/material.dart';

import 'package:ohanami/widgets/button_blue.dart';
import 'package:ohanami/widgets/card_input.dart';

class Round3 extends StatefulWidget {
  const Round3({
    Key? key,
    required this.totalJugadores,
    required this.jugador2,
    required this.jugador3,
    required this.jugador4,
  }) : super(key: key);

  final num totalJugadores;
  final String jugador2;
  final String jugador3;
  final String jugador4;

  @override
  State<Round3> createState() => _Round3State();
}

class _Round3State extends State<Round3> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: ConstrainedBox(
            constraints: BoxConstraints(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                padding: EdgeInsets.symmetric(horizontal: 50),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: Row(
                            children: [
                              IconButton(
                                icon: new Icon(Icons.arrow_back),
                                highlightColor: Colors.blue,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              )
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(bottom: 5),
                              child: Text("Ronda 3", style: TextStyle(fontSize: 20),),
                            ),
                            Round3Content(count: widget.totalJugadores, jugador2: widget.jugador2, jugador3: widget.jugador3, jugador4: widget.jugador4),
                            ButtonBlue(
                              text: 'Confirmar Cantida de Cartas', 
                              onPressed: () async {
                                FocusScope.of(context).unfocus();
                                // print(jugador2Ctrl.text);
                                // print(jugador3Ctrl.text);
                                // print(jugador4Ctrl.text);

                                // if(jugador2Ctrl.text == '') {
                                //   showAlert(context, 'Oops, no puedes hacer eso', 'El jugador 2 es necesario');
                                // } else if(jugador3Ctrl.text == '' && jugador4Ctrl.text != '') {
                                //   showAlert(context, 'Oops, no puedes hacer eso', 'Tienes que escribir el nombre del tercer jugador también');
                                // } else {
                                //   String texto = jugador2Ctrl.text;
                                //   Navigator.push(context, 
                                //     MaterialPageRoute(builder: (context) => Round1()));
                                // }


                                // final registerOk = await authService.register(nameCtrl.text.trim(), emailCtrl.text.trim(), passCtrl.text.trim());
                
                                // if(registerOk == true) {
                                //   Navigator.pushReplacementNamed(context, 'users');
                                // } else {
                                //   showAlert(context, 'Registro incorrecto', registerOk);
                                // }
                              }
                            ),
                          ],
                        )
                      ],
                    ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Round3Content extends StatefulWidget {
  const Round3Content({
    Key? key,
    required this.count,
    required this.jugador2,
    required this.jugador3,
    required this.jugador4,
  }) : super(key: key);

  final num count;
  final String jugador2;
  final String jugador3;
  final String jugador4;

  @override
  _Round3ContentState createState() => _Round3ContentState();
}

class _Round3ContentState extends State<Round3Content> {

  final jugador1Ctrl = TextEditingController();
  final jugador2Ctrl = TextEditingController();
  final jugador3Ctrl = TextEditingController();
  final jugador4Ctrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    if (widget.count == 2) {
      return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text("Tú", style: TextStyle(fontSize: 20),),
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: jugador1Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: jugador1Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: jugador1Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: jugador1Ctrl,
          isPassword: false,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        )
        ]
      );
    } else if (widget.count == 3) {
      return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text("Tú", style: TextStyle(fontSize: 20),),
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: jugador1Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: jugador1Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: jugador1Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: jugador1Ctrl,
          isPassword: false,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
          Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador3, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: jugador3Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: jugador3Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: jugador3Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: jugador3Ctrl,
          isPassword: false,
        ),
        ]
      );
    } else if (widget.count == 4) {
      return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text("Tú", style: TextStyle(fontSize: 20),),
        ),
       CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: jugador1Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: jugador1Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
          Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador3, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
          Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador4, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: jugador2Ctrl,
          isPassword: false,
        ),
        ]
      );
    }
    return Container();
  }
}