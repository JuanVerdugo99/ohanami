import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class StatsPage extends StatefulWidget {
 
  @override
  State<StatsPage> createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage> {
  late List<GDPData> _chartData;

  late TooltipBehavior _tooltipBehavior;

  @override
  void initState() {
    _chartData = getChartData();
    _tooltipBehavior = TooltipBehavior(enable: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SfCircularChart(
        title: ChartTitle(text: 'Tus Estadísticas'),
        legend: Legend(isVisible: true, overflowMode: LegendItemOverflowMode.wrap),
        tooltipBehavior: _tooltipBehavior,
        palette: [
          Colors.lightBlue,
          Colors.green,
          Colors.grey,
          Colors.pinkAccent
        ],
        series: <CircularSeries>[ 
          DoughnutSeries<GDPData, String>(
            dataSource: _chartData,
            xValueMapper: (GDPData data, _) => data.continent,
            yValueMapper: (GDPData data, _) => data.gdp,
            dataLabelSettings: DataLabelSettings(isVisible: true),
            enableTooltip: true
          )],);
  }

  List<GDPData> getChartData() {
    final List<GDPData> chartData = [
      GDPData(gdp: 25, continent: 'Total Azules'),
      GDPData(gdp: 78, continent: 'Total Verdes'),
      GDPData(gdp: 51, continent: 'Total Grises'),
      GDPData(gdp: 51, continent: 'Total Rosas'),
    ];
    return chartData;
  }
}

class GDPData {
  final int gdp;
  final String continent;
  GDPData({
    required this.gdp,
    required this.continent,
  });
}