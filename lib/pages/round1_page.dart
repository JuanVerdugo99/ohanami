import 'package:flutter/material.dart';
import 'package:ohanami/helpers/show_alert.dart';
import 'package:ohanami/models/partidas.dart';
import 'package:ohanami/pages/partidas_page.dart';

import 'package:ohanami/pages/round2_page.dart';
import 'package:ohanami/pages/users_page.dart';
import 'package:ohanami/services/auth_service.dart';
import 'package:ohanami/widgets/button_blue.dart';
import 'package:ohanami/widgets/card_input.dart';
import 'package:provider/provider.dart';

class Round1 extends StatefulWidget {
  const Round1({
    Key? key,
    required this.jugador2,
    required this.jugador3,
    required this.jugador4,
    required this.totalJugadores,
  }) : super(key: key);

  final String jugador2;
  final String jugador3;
  final String jugador4;
  final num totalJugadores;

  @override
  State<Round1> createState() => _Round1State();
}

class _Round1State extends State<Round1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: ConstrainedBox(
            constraints: BoxConstraints(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                padding: EdgeInsets.symmetric(horizontal: 50),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: Row(
                            children: [
                              IconButton(
                                icon: new Icon(Icons.arrow_back),
                                highlightColor: Colors.blue,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              )
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(22.0),
                              child: Text("Ronda 1", style: TextStyle(fontSize: 20),),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 160),
                              child: Round1Content(count: widget.totalJugadores, jugador2: widget.jugador2, jugador3: widget.jugador3, jugador4: widget.jugador4,),
                            ),
                            // CardInput(
                            //   icon: Icons.web_stories,
                            //   color: Colors.blue,
                            //   placeholder: 'Cantidad de Cartas Azules',
                            //   keyboardType: TextInputType.text,
                            //   textController: jugador3Ctrl,
                            //   isPassword: false,
                            // ),
                            // CardInput(
                            //   icon: Icons.web_stories,
                            //   color: Colors.blue,
                            //   placeholder: 'Cantidad de Cartas Azules',
                            //   keyboardType: TextInputType.text,
                            //   textController: jugador4Ctrl,
                            //   isPassword: false,
                            // ),
                            // ButtonBlue(
                            //   text: 'Confirmar', 
                            //   onPressed: () async {
                            //     FocusScope.of(context).unfocus();
                            //     // print(jugador2Ctrl.text);
                            //     // print(jugador3Ctrl.text);
                            //     // print(jugador4Ctrl.text);

                            //      Navigator.push(context, 
                            //         MaterialPageRoute(builder: (context) => Round2(jugador2: widget.jugador2, jugador3: widget.jugador3, jugador4: widget.jugador4, totalJugadores: widget.totalJugadores,)));

                                // if(jugador2Ctrl.text == '') {
                                //   showAlert(context, 'Oops, no puedes hacer eso', 'El jugador 2 es necesario');
                                // } else if(jugador3Ctrl.text == '' && jugador4Ctrl.text != '') {
                                //   showAlert(context, 'Oops, no puedes hacer eso', 'Tienes que escribir el nombre del tercer jugador también');
                                // } else {
                                //   String texto = jugador2Ctrl.text;
                                //   Navigator.push(context, 
                                //     MaterialPageRoute(builder: (context) => Round1()));
                                // }


                                // final registerOk = await authService.register(nameCtrl.text.trim(), emailCtrl.text.trim(), passCtrl.text.trim());
                
                                // if(registerOk == true) {
                                //   Navigator.pushReplacementNamed(context, 'users');
                                // } else {
                                //   showAlert(context, 'Registro incorrecto', registerOk);
                                // }
                              // }
                            // ),
                          ],
                        )
                      ],
                    ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Round1Content extends StatefulWidget {
  Round1Content({
    Key? key,
    required this.count,
    required this.jugador2,
    required this.jugador3,
    required this.jugador4,
  }) : super(key: key);

  final num count;
  final String jugador2;
  final String jugador3;
  final String jugador4;

  @override
  _Round1ContentState createState() => _Round1ContentState();
}

class _Round1ContentState extends State<Round1Content> {

  // Ronda 1
  final azulesRonda1Jugador1 = TextEditingController();
  final azulesRonda1Jugador2 = TextEditingController();
  final azulesRonda1Jugador3 = TextEditingController();
  final azulesRonda1Jugador4 = TextEditingController();

  // Ronda 2
  final azulesRonda2Jugador1 = TextEditingController();
  final azulesRonda2Jugador2 = TextEditingController();
  final azulesRonda2Jugador3 = TextEditingController();
  final azulesRonda2Jugador4 = TextEditingController();

  final verdesRonda2Jugador1 = TextEditingController();
  final verdesRonda2Jugador2 = TextEditingController();
  final verdesRonda2Jugador3 = TextEditingController();
  final verdesRonda2Jugador4 = TextEditingController();

  // Ronda 3
  final azulesRonda3Jugador1 = TextEditingController();
  final azulesRonda3Jugador2 = TextEditingController();
  final azulesRonda3Jugador3 = TextEditingController();
  final azulesRonda3Jugador4 = TextEditingController();

  final verdesRonda3Jugador1 = TextEditingController();
  final verdesRonda3Jugador2 = TextEditingController();
  final verdesRonda3Jugador3 = TextEditingController();
  final verdesRonda3Jugador4 = TextEditingController();

  final grisesRonda3Jugador1 = TextEditingController();
  final grisesRonda3Jugador2 = TextEditingController();
  final grisesRonda3Jugador3 = TextEditingController();
  final grisesRonda3Jugador4 = TextEditingController();

  final rosasRonda3Jugador1 = TextEditingController();
  final rosasRonda3Jugador2 = TextEditingController();
  final rosasRonda3Jugador3 = TextEditingController();
  final rosasRonda3Jugador4 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    final user = authService.user;
    
    if (widget.count == 2) {
      return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text("Tú", style: TextStyle(fontSize: 20),),
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda1Jugador1,
          isPassword: false,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda1Jugador2,
          isPassword: false,
          ),
          Padding(
            padding: const EdgeInsets.all(22.0),
            child: Text("Ronda 2", style: TextStyle(fontSize: 20),),
          ),
           Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Text("Tú", style: TextStyle(fontSize: 20),),
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.blue,
              placeholder: 'Cantidad de Cartas Azules ',
              keyboardType: TextInputType.text,
              textController: azulesRonda2Jugador1,
              isPassword: false,
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.green,
              placeholder: 'Cantidad de Cartas Verdes ',
              keyboardType: TextInputType.text,
              textController: verdesRonda2Jugador1,
              isPassword: false,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.blue,
              placeholder: 'Cantidad de Cartas Azules ',
              keyboardType: TextInputType.text,
              textController: azulesRonda2Jugador2,
              isPassword: false,
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.green,
              placeholder: 'Cantidad de Cartas Verdes',
              keyboardType: TextInputType.text,
              textController: verdesRonda2Jugador2,
              isPassword: false,
            ),
          Padding(
            padding: const EdgeInsets.all(22.0),
            child: Text("Ronda 3", style: TextStyle(fontSize: 20),),
          ),
          Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text("Tú", style: TextStyle(fontSize: 20),),
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda3Jugador1,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: verdesRonda3Jugador1,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: grisesRonda3Jugador1,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: rosasRonda3Jugador1,
          isPassword: false,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda3Jugador2,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: verdesRonda3Jugador2,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: grisesRonda3Jugador2,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: rosasRonda3Jugador2,
          isPassword: false,
        ),
        ButtonBlue(
        text: 'Confirmar', 
        onPressed: () async {
          FocusScope.of(context).unfocus();
          
          try {
            List<int> rosasCantidad = [0,1,3,6,10,15,21,28,36,45,55,66,78,91,105,120];

          int cantidadAzulesRonda1Jugador1 = 0;
          int cantidadAzulesRonda1Jugador2 = 0;

          int cantidadAzulesRonda2Jugador1 = 0;
          int cantidadAzulesRonda2Jugador2 = 0;
          int cantidadVerdesRonda2Jugador1 = 0;
          int cantidadVerdesRonda2Jugador2 = 0;

          int cantidadAzulesRonda3Jugador1 = 0;
          int cantidadAzulesRonda3Jugador2 = 0;
          int cantidadVerdesRonda3Jugador1 = 0;
          int cantidadVerdesRonda3Jugador2 = 0;
          int cantidadGrisesRonda3Jugador1 = 0;
          int cantidadGrisesRonda3Jugador2 = 0;
          int cantidadRosasRonda3Jugador1 = 0;
          int cantidadRosasRonda3Jugador2 = 0;

          int totalJugador1 = 0;
          int totalJugador2 = 0;

          // Ronda 1
          cantidadAzulesRonda1Jugador1 = 3 * int.parse(azulesRonda1Jugador1.text);
          cantidadAzulesRonda1Jugador2 = 3 * int.parse(azulesRonda1Jugador2.text);

          // Ronda 2
          cantidadAzulesRonda2Jugador1 = 3 * int.parse(azulesRonda2Jugador1.text);
          cantidadAzulesRonda2Jugador2 = 3 * int.parse(azulesRonda2Jugador2.text);
          cantidadVerdesRonda2Jugador1 = 4 * int.parse(verdesRonda2Jugador1.text);
          cantidadVerdesRonda2Jugador2 = 4 * int.parse(verdesRonda2Jugador2.text);

          // Ronda 3
          cantidadAzulesRonda3Jugador1 = 3 * int.parse(azulesRonda3Jugador1.text);
          cantidadAzulesRonda3Jugador2 = 3 * int.parse(azulesRonda3Jugador2.text);
          cantidadVerdesRonda3Jugador1 = 4 * int.parse(verdesRonda3Jugador1.text);
          cantidadVerdesRonda3Jugador2 = 4 * int.parse(verdesRonda3Jugador2.text);
          cantidadGrisesRonda3Jugador1 = 7 * int.parse(grisesRonda3Jugador1.text);
          cantidadGrisesRonda3Jugador2 = 7 * int.parse(grisesRonda3Jugador2.text);

        double totalCartasAzules = 0;
        double totalCartasVerdes = 0;
        double totalCartasGrises = 0;
        int totalCartasRosas = 0;

        totalCartasAzules =  (cantidadAzulesRonda1Jugador1 + cantidadAzulesRonda2Jugador1 +  cantidadAzulesRonda3Jugador1) / 3; 
        totalCartasVerdes =  (cantidadVerdesRonda2Jugador1 +  cantidadVerdesRonda3Jugador1) / 4; 
        totalCartasGrises =  cantidadGrisesRonda3Jugador1 / 7; 
        totalCartasRosas =  int.parse(rosasRonda3Jugador1.text);

          if(int.parse(rosasRonda3Jugador1.text) > 15) {
            cantidadRosasRonda3Jugador1 = 120;
          } else {
            cantidadRosasRonda3Jugador1 = rosasCantidad[int.parse(rosasRonda3Jugador1.text)];
          }

          if(int.parse(rosasRonda3Jugador2.text) > 15) {
            cantidadRosasRonda3Jugador2 = 120;
          } else {
            cantidadRosasRonda3Jugador2 = rosasCantidad[int.parse(rosasRonda3Jugador2.text)];
          }

        totalJugador1 = 
          cantidadAzulesRonda1Jugador1 +
          cantidadAzulesRonda2Jugador1 +
          cantidadVerdesRonda2Jugador1 +
          cantidadAzulesRonda3Jugador1 + 
          cantidadVerdesRonda3Jugador1 + 
          cantidadGrisesRonda3Jugador1 +
          cantidadRosasRonda3Jugador1;
      
        totalJugador2 = 
          cantidadAzulesRonda1Jugador2 +
          cantidadAzulesRonda2Jugador2 +
          cantidadVerdesRonda2Jugador2 +
          cantidadAzulesRonda3Jugador2 + 
          cantidadVerdesRonda3Jugador2 + 
          cantidadGrisesRonda3Jugador2 +
          cantidadRosasRonda3Jugador2;

          // Partida body = {
          //   'jugador1': user.name,
          //   'jugador2': widget.jugador2,
          //   'jugador3': '',
          //   'jugador4': '',
          //   'totalJugador1': 0,
          //   'totalJugador2': 0,
          //   'totalJugador3': 0,
          //   'totalJugador4': 0,
          //   'fecha': DateTime
          // };

          // final roundOk = await authService.crearPartida(Partida(jugador1: user.name, jugador2: widget.jugador2, jugador3: '', jugador4: '', totalJugador1: totalJugador1, totalJugador2: totalJugador2, totalJugador3: 0, totalJugador4: 0));
          // await authService.crearPartida(Partida(jugadores: 
          //                                         Jugadores(
          //                                           jugador1: user.name, 
          //                                           jugador2: widget.jugador2, 
          //                                           jugador3: widget.jugador3, 
          //                                           jugador4: widget.jugador4
          //                                         ), 
          //                                         puntuaciones: Puntuaciones(  
          //                                            totalJugador1: totalJugador1,
          //                                            totalJugador2: totalJugador2,
          //                                            totalJugador3: 0,
          //                                            totalJugador4: 0), 
          //                                         idUser: user.uid));

          // await authService.crearPartida(Partida(idUser: user.uid, 
          //                                        jugadores: [Jugadore(jugador: user.name, puntuacion: totalJugador1), 
          //                                                   Jugadore(jugador: widget.jugador2, puntuacion: totalJugador2)], ));

          await authService.crearPartida(user.uid, user.name, widget.jugador2, totalJugador1, totalJugador2, totalCartasAzules, totalCartasVerdes, totalCartasGrises, totalCartasRosas);
          
          Navigator.push(context, 
              MaterialPageRoute(builder: (context) => UsersPage()));
          } catch (e) {
            showAlert(context, 'Llene todas las casillas', 'porfavor');
          }

          
            }
          )
        ]
      );
    } else if (widget.count == 3) {
      return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text("Tú", style: TextStyle(fontSize: 20),),
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda1Jugador1,
          isPassword: false,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda1Jugador2,
          isPassword: false,
          ),
          Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador3, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda1Jugador3,
          isPassword: false,
          ),
          Padding(
            padding: const EdgeInsets.all(22.0),
            child: Text("Ronda 2", style: TextStyle(fontSize: 20),),
          ),
          Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Text("Tú", style: TextStyle(fontSize: 20),),
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.blue,
              placeholder: 'Cantidad de Cartas Azules ',
              keyboardType: TextInputType.text,
              textController: azulesRonda2Jugador1,
              isPassword: false,
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.green,
              placeholder: 'Cantidad de Cartas Verdes ',
              keyboardType: TextInputType.text,
              textController: verdesRonda2Jugador1,
              isPassword: false,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.blue,
              placeholder: 'Cantidad de Cartas Azules ',
              keyboardType: TextInputType.text,
              textController: azulesRonda2Jugador2,
              isPassword: false,
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.green,
              placeholder: 'Cantidad de Cartas Verdes',
              keyboardType: TextInputType.text,
              textController: verdesRonda2Jugador2,
              isPassword: false,
            ),
      Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Text(widget.jugador3, style: TextStyle(fontSize: 20),),
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.blue,
              placeholder: 'Cantidad de Cartas Azules ',
              keyboardType: TextInputType.text,
              textController: azulesRonda2Jugador3,
              isPassword: false,
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.green,
              placeholder: 'Cantidad de Cartas Verdes',
              keyboardType: TextInputType.text,
              textController: verdesRonda2Jugador3,
              isPassword: false,
            ),
             Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Text("Ronda 3", style: TextStyle(fontSize: 20),),
            ),
            Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text("Tú", style: TextStyle(fontSize: 20),),
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda3Jugador1,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: verdesRonda3Jugador1,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: grisesRonda3Jugador1,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: rosasRonda3Jugador1,
          isPassword: false,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda3Jugador2,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: verdesRonda3Jugador2,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: grisesRonda3Jugador2,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: rosasRonda3Jugador2,
          isPassword: false,
        ),
          Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador3, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda3Jugador3,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: verdesRonda3Jugador3,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: grisesRonda3Jugador3,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: rosasRonda3Jugador3,
          isPassword: false,
        ),
        ButtonBlue(
        text: 'Confirmar', 
        onPressed: () async {
          FocusScope.of(context).unfocus();
         try {
            List<int> rosasCantidad = [0,1,3,6,10,15,21,28,36,45,55,66,78,91,105,120];

          int cantidadAzulesRonda1Jugador1 = 0;
          int cantidadAzulesRonda1Jugador2 = 0;
          int cantidadAzulesRonda1Jugador3 = 0;

          int cantidadAzulesRonda2Jugador1 = 0;
          int cantidadAzulesRonda2Jugador2 = 0;
          int cantidadAzulesRonda2Jugador3 = 0;
          int cantidadVerdesRonda2Jugador1 = 0;
          int cantidadVerdesRonda2Jugador2 = 0;
          int cantidadVerdesRonda2Jugador3 = 0;

          int cantidadAzulesRonda3Jugador1 = 0;
          int cantidadAzulesRonda3Jugador2 = 0;
          int cantidadAzulesRonda3Jugador3 = 0;
          int cantidadVerdesRonda3Jugador1 = 0;
          int cantidadVerdesRonda3Jugador2 = 0;
          int cantidadVerdesRonda3Jugador3 = 0;
          int cantidadGrisesRonda3Jugador1 = 0;
          int cantidadGrisesRonda3Jugador2 = 0;
          int cantidadGrisesRonda3Jugador3 = 0;
          int cantidadRosasRonda3Jugador1 = 0;
          int cantidadRosasRonda3Jugador2 = 0;
          int cantidadRosasRonda3Jugador3 = 0;

          int totalJugador1 = 0;
          int totalJugador2 = 0;
          int totalJugador3 = 0;

          // Ronda 1
          cantidadAzulesRonda1Jugador1 = 3 * int.parse(azulesRonda1Jugador1.text);
          cantidadAzulesRonda1Jugador2 = 3 * int.parse(azulesRonda1Jugador2.text);
          cantidadAzulesRonda1Jugador3 = 3 * int.parse(azulesRonda1Jugador3.text);

          // Ronda 2
          cantidadAzulesRonda2Jugador1 = 3 * int.parse(azulesRonda2Jugador1.text);
          cantidadAzulesRonda2Jugador2 = 3 * int.parse(azulesRonda2Jugador2.text);
          cantidadAzulesRonda2Jugador3 = 3 * int.parse(azulesRonda2Jugador3.text);
          cantidadVerdesRonda2Jugador1 = 4 * int.parse(verdesRonda2Jugador1.text);
          cantidadVerdesRonda2Jugador2 = 4 * int.parse(verdesRonda2Jugador2.text);
          cantidadVerdesRonda2Jugador3 = 4 * int.parse(verdesRonda2Jugador3.text);

          // Ronda 3
          cantidadAzulesRonda3Jugador1 = 3 * int.parse(azulesRonda3Jugador1.text);
          cantidadAzulesRonda3Jugador2 = 3 * int.parse(azulesRonda3Jugador2.text);
          cantidadAzulesRonda3Jugador3 = 3 * int.parse(azulesRonda3Jugador3.text);
          cantidadVerdesRonda3Jugador1 = 4 * int.parse(verdesRonda3Jugador1.text);
          cantidadVerdesRonda3Jugador2 = 4 * int.parse(verdesRonda3Jugador2.text);
          cantidadVerdesRonda3Jugador3 = 4 * int.parse(verdesRonda3Jugador3.text);
          cantidadGrisesRonda3Jugador1 = 7 * int.parse(grisesRonda3Jugador1.text);
          cantidadGrisesRonda3Jugador2 = 7 * int.parse(grisesRonda3Jugador2.text);
          cantidadGrisesRonda3Jugador3 = 7 * int.parse(grisesRonda3Jugador3.text);

        double totalCartasAzules = 0;
        double totalCartasVerdes = 0;
        double totalCartasGrises = 0;
        int totalCartasRosas = 0;

        totalCartasAzules =  (cantidadAzulesRonda1Jugador1 + cantidadAzulesRonda2Jugador1 +  cantidadAzulesRonda3Jugador1) / 3; 
        totalCartasVerdes =  (cantidadVerdesRonda2Jugador1 +  cantidadVerdesRonda3Jugador1) / 4; 
        totalCartasGrises =  cantidadGrisesRonda3Jugador1 / 7; 
        totalCartasRosas =  int.parse(rosasRonda3Jugador1.text);
          
          if(int.parse(rosasRonda3Jugador1.text) > 15) {
            cantidadRosasRonda3Jugador1 = 120;
          } else {
            cantidadRosasRonda3Jugador1 = rosasCantidad[int.parse(rosasRonda3Jugador1.text)];
          }

          if(int.parse(rosasRonda3Jugador2.text) > 15) {
            cantidadRosasRonda3Jugador2 = 120;
          } else {
            cantidadRosasRonda3Jugador2 = rosasCantidad[int.parse(rosasRonda3Jugador2.text)];
          }

          if(int.parse(rosasRonda3Jugador3.text) > 15) {
            cantidadRosasRonda3Jugador3 = 120;
          } else {
            cantidadRosasRonda3Jugador3 = rosasCantidad[int.parse(rosasRonda3Jugador3.text)];
          }

        totalJugador1 = 
          cantidadAzulesRonda1Jugador1 +
          cantidadAzulesRonda2Jugador1 +
          cantidadVerdesRonda2Jugador1 +
          cantidadAzulesRonda3Jugador1 + 
          cantidadVerdesRonda3Jugador1 + 
          cantidadGrisesRonda3Jugador1 +
          cantidadRosasRonda3Jugador1;


        totalJugador2 = 
          cantidadAzulesRonda1Jugador2 +
          cantidadAzulesRonda2Jugador2 +
          cantidadVerdesRonda2Jugador2 +
          cantidadAzulesRonda3Jugador2 + 
          cantidadVerdesRonda3Jugador2 + 
          cantidadGrisesRonda3Jugador2 +
          cantidadRosasRonda3Jugador2;

        totalJugador3 = 
          cantidadAzulesRonda1Jugador3 +
          cantidadAzulesRonda2Jugador3 +
          cantidadVerdesRonda2Jugador3 +
          cantidadAzulesRonda3Jugador3 + 
          cantidadVerdesRonda3Jugador3 + 
          cantidadGrisesRonda3Jugador3 +
          cantidadRosasRonda3Jugador3;


          await authService.crearPartida(user.uid, user.name, widget.jugador2, totalJugador1, totalJugador2, totalCartasAzules, totalCartasVerdes, totalCartasGrises, totalCartasRosas,
                                          widget.jugador3, totalJugador3);
          
          Navigator.push(context, 
              MaterialPageRoute(builder: (context) => UsersPage()));
          } catch (e) {
            showAlert(context, 'Llene todas las casillas', 'porfavor');
          }
            }
          )
        ]
      );
    } else if (widget.count == 4) {
      return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text("Tú", style: TextStyle(fontSize: 20),),
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda1Jugador1,
          isPassword: false,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda1Jugador2,
          isPassword: false,
          ),
          Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador3, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda1Jugador3,
          isPassword: false,
          ),
          Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador4, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda1Jugador4,
          isPassword: false,
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 5),
            child: Text("Ronda 2", style: TextStyle(fontSize: 20),),
          ),
          Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Text("Tú", style: TextStyle(fontSize: 20),),
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.blue,
              placeholder: 'Cantidad de Cartas Azules ',
              keyboardType: TextInputType.text,
              textController: azulesRonda2Jugador1,
              isPassword: false,
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.green,
              placeholder: 'Cantidad de Cartas Verdes ',
              keyboardType: TextInputType.text,
              textController: verdesRonda2Jugador1,
              isPassword: false,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.blue,
              placeholder: 'Cantidad de Cartas Azules ',
              keyboardType: TextInputType.text,
              textController: azulesRonda2Jugador2,
              isPassword: false,
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.green,
              placeholder: 'Cantidad de Cartas Verdes',
              keyboardType: TextInputType.text,
              textController: verdesRonda2Jugador2,
              isPassword: false,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Text(widget.jugador3, style: TextStyle(fontSize: 20),),
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.blue,
              placeholder: 'Cantidad de Cartas Azules ',
              keyboardType: TextInputType.text,
              textController: azulesRonda2Jugador3,
              isPassword: false,
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.green,
              placeholder: 'Cantidad de Cartas Verdes',
              keyboardType: TextInputType.text,
              textController: verdesRonda2Jugador3,
              isPassword: false,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Text(widget.jugador4, style: TextStyle(fontSize: 20),),
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.blue,
              placeholder: 'Cantidad de Cartas Azules ',
              keyboardType: TextInputType.text,
              textController: azulesRonda2Jugador4,
              isPassword: false,
            ),
            CardInput(
              icon: Icons.web_stories,
              color: Colors.green,
              placeholder: 'Cantidad de Cartas Verdes',
              keyboardType: TextInputType.text,
              textController: verdesRonda2Jugador4,
              isPassword: false,
            ),
            Padding(
            padding: EdgeInsets.only(bottom: 5),
            child: Text("Ronda 3", style: TextStyle(fontSize: 20),),
          ),
          Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text("Tú", style: TextStyle(fontSize: 20),),
        ),
       CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda3Jugador1,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: verdesRonda3Jugador1,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: grisesRonda3Jugador1,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: rosasRonda3Jugador1,
          isPassword: false,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador2, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda3Jugador2,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: verdesRonda3Jugador2,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: grisesRonda3Jugador2,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: rosasRonda3Jugador2,
          isPassword: false,
        ),
          Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador3, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda3Jugador3,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: verdesRonda3Jugador3,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: grisesRonda3Jugador3,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: rosasRonda3Jugador3,
          isPassword: false,
        ),
          Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(widget.jugador4, style: TextStyle(fontSize: 20),),
        ),
      CardInput(
          icon: Icons.web_stories,
          color: Colors.blue,
          placeholder: 'Cantidad de Cartas Azules ',
          keyboardType: TextInputType.text,
          textController: azulesRonda3Jugador4,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.green,
          placeholder: 'Cantidad de Cartas Verdes ',
          keyboardType: TextInputType.text,
          textController: verdesRonda3Jugador4,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.grey,
          placeholder: 'Cantidad de Cartas Grises ',
          keyboardType: TextInputType.text,
          textController: grisesRonda3Jugador4,
          isPassword: false,
        ),
        CardInput(
          icon: Icons.web_stories,
          color: Colors.pinkAccent,
          placeholder: 'Cantidad de Cartas Rosas ',
          keyboardType: TextInputType.text,
          textController: rosasRonda3Jugador4,
          isPassword: false,
        ),
        ButtonBlue(
        text: 'Confirmar', 
        onPressed: () async {
          FocusScope.of(context).unfocus();
          try {
            List<int> rosasCantidad = [0,1,3,6,10,15,21,28,36,45,55,66,78,91,105,120];

          int cantidadAzulesRonda1Jugador1 = 0;
          int cantidadAzulesRonda1Jugador2 = 0;
          int cantidadAzulesRonda1Jugador3 = 0;
          int cantidadAzulesRonda1Jugador4 = 0;

          int cantidadAzulesRonda2Jugador1 = 0;
          int cantidadAzulesRonda2Jugador2 = 0;
          int cantidadAzulesRonda2Jugador3 = 0;
          int cantidadAzulesRonda2Jugador4 = 0;
          int cantidadVerdesRonda2Jugador1 = 0;
          int cantidadVerdesRonda2Jugador2 = 0;
          int cantidadVerdesRonda2Jugador3 = 0;
          int cantidadVerdesRonda2Jugador4 = 0;

          int cantidadAzulesRonda3Jugador1 = 0;
          int cantidadAzulesRonda3Jugador2 = 0;
          int cantidadAzulesRonda3Jugador3 = 0;
          int cantidadAzulesRonda3Jugador4 = 0;
          int cantidadVerdesRonda3Jugador1 = 0;
          int cantidadVerdesRonda3Jugador2 = 0;
          int cantidadVerdesRonda3Jugador3 = 0;
          int cantidadVerdesRonda3Jugador4 = 0;
          int cantidadGrisesRonda3Jugador1 = 0;
          int cantidadGrisesRonda3Jugador2 = 0;
          int cantidadGrisesRonda3Jugador3 = 0; 
          int cantidadGrisesRonda3Jugador4 = 0; 
          int cantidadRosasRonda3Jugador1 = 0;
          int cantidadRosasRonda3Jugador2 = 0;
          int cantidadRosasRonda3Jugador3 = 0;
          int cantidadRosasRonda3Jugador4 = 0;

          int totalJugador1 = 0;
          int totalJugador2 = 0;
          int totalJugador3 = 0;
          int totalJugador4 = 0;

          // Ronda 1
          cantidadAzulesRonda1Jugador1 = 3 * int.parse(azulesRonda1Jugador1.text);
          cantidadAzulesRonda1Jugador2 = 3 * int.parse(azulesRonda1Jugador2.text);
          cantidadAzulesRonda1Jugador3 = 3 * int.parse(azulesRonda1Jugador3.text);
          cantidadAzulesRonda1Jugador4 = 3 * int.parse(azulesRonda1Jugador4.text);

          // Ronda 2
          cantidadAzulesRonda2Jugador1 = 3 * int.parse(azulesRonda2Jugador1.text);
          cantidadAzulesRonda2Jugador2 = 3 * int.parse(azulesRonda2Jugador2.text);
          cantidadAzulesRonda2Jugador3 = 3 * int.parse(azulesRonda2Jugador3.text);
          cantidadAzulesRonda2Jugador4 = 3 * int.parse(azulesRonda2Jugador4.text);
          cantidadVerdesRonda2Jugador1 = 4 * int.parse(verdesRonda2Jugador1.text);
          cantidadVerdesRonda2Jugador2 = 4 * int.parse(verdesRonda2Jugador2.text);
          cantidadVerdesRonda2Jugador3 = 4 * int.parse(verdesRonda2Jugador3.text);
          cantidadVerdesRonda2Jugador4 = 4 * int.parse(verdesRonda2Jugador4.text);

          // Ronda 3
          cantidadAzulesRonda3Jugador1 = 3 * int.parse(azulesRonda3Jugador1.text);
          cantidadAzulesRonda3Jugador2 = 3 * int.parse(azulesRonda3Jugador2.text);
          cantidadAzulesRonda3Jugador3 = 3 * int.parse(azulesRonda3Jugador3.text);
          cantidadAzulesRonda3Jugador4 = 3 * int.parse(azulesRonda3Jugador4.text);
          cantidadVerdesRonda3Jugador1 = 4 * int.parse(verdesRonda3Jugador1.text);
          cantidadVerdesRonda3Jugador2 = 4 * int.parse(verdesRonda3Jugador2.text);
          cantidadVerdesRonda3Jugador3 = 4 * int.parse(verdesRonda3Jugador3.text);
          cantidadVerdesRonda3Jugador4 = 4 * int.parse(verdesRonda3Jugador4.text);
          cantidadGrisesRonda3Jugador1 = 7 * int.parse(grisesRonda3Jugador1.text);
          cantidadGrisesRonda3Jugador2 = 7 * int.parse(grisesRonda3Jugador2.text);
          cantidadGrisesRonda3Jugador3 = 7 * int.parse(grisesRonda3Jugador3.text);
          cantidadGrisesRonda3Jugador4 = 7 * int.parse(grisesRonda3Jugador4.text);

        double totalCartasAzules = 0;
        double totalCartasVerdes = 0;
        double totalCartasGrises = 0;
        int totalCartasRosas = 0;

        totalCartasAzules =  (cantidadAzulesRonda1Jugador1 + cantidadAzulesRonda2Jugador1 +  cantidadAzulesRonda3Jugador1) / 3; 
        totalCartasVerdes =  (cantidadVerdesRonda2Jugador1 +  cantidadVerdesRonda3Jugador1) / 4; 
        totalCartasGrises =  cantidadGrisesRonda3Jugador1 / 7; 
        totalCartasRosas =  int.parse(rosasRonda3Jugador1.text);
          
          if(int.parse(rosasRonda3Jugador1.text) > 15) {
            cantidadRosasRonda3Jugador1 = 120;
          } else {
            cantidadRosasRonda3Jugador1 = rosasCantidad[int.parse(rosasRonda3Jugador1.text)];
          }

          if(int.parse(rosasRonda3Jugador2.text) > 15) {
            cantidadRosasRonda3Jugador2 = 120;
          } else {
            cantidadRosasRonda3Jugador2 = rosasCantidad[int.parse(rosasRonda3Jugador2.text)];
          }

          if(int.parse(rosasRonda3Jugador3.text) > 15) {
            cantidadRosasRonda3Jugador3 = 120;
          } else {
            cantidadRosasRonda3Jugador3 = rosasCantidad[int.parse(rosasRonda3Jugador3.text)];
          }

          if(int.parse(rosasRonda3Jugador4.text) > 15) {
            cantidadRosasRonda3Jugador4 = 120;
          } else {
            cantidadRosasRonda3Jugador4 = rosasCantidad[int.parse(rosasRonda3Jugador4.text)];
          }

        totalJugador1 = 
          cantidadAzulesRonda1Jugador1 +
          cantidadAzulesRonda2Jugador1 +
          cantidadVerdesRonda2Jugador1 +
          cantidadAzulesRonda3Jugador1 + 
          cantidadVerdesRonda3Jugador1 + 
          cantidadGrisesRonda3Jugador1 +
          cantidadRosasRonda3Jugador1;


        totalJugador2 = 
          cantidadAzulesRonda1Jugador2 +
          cantidadAzulesRonda2Jugador2 +
          cantidadVerdesRonda2Jugador2 +
          cantidadAzulesRonda3Jugador2 + 
          cantidadVerdesRonda3Jugador2 + 
          cantidadGrisesRonda3Jugador2 +
          cantidadRosasRonda3Jugador2;

        totalJugador3 = 
          cantidadAzulesRonda1Jugador3 +
          cantidadAzulesRonda2Jugador3 +
          cantidadVerdesRonda2Jugador3 +
          cantidadAzulesRonda3Jugador3 + 
          cantidadVerdesRonda3Jugador3 + 
          cantidadGrisesRonda3Jugador3 +
          cantidadRosasRonda3Jugador3;

          totalJugador4 = 
          cantidadAzulesRonda1Jugador4 +
          cantidadAzulesRonda2Jugador4 +
          cantidadVerdesRonda2Jugador4 +
          cantidadAzulesRonda3Jugador4 + 
          cantidadVerdesRonda3Jugador4 + 
          cantidadGrisesRonda3Jugador4 +
          cantidadRosasRonda3Jugador4;

         await authService.crearPartida(user.uid, user.name, widget.jugador2, totalJugador1, totalJugador2,totalCartasAzules, totalCartasVerdes, totalCartasGrises, totalCartasRosas,
                                          widget.jugador3, totalJugador3, widget.jugador4, totalJugador4);
          
          Navigator.push(context, 
              MaterialPageRoute(builder: (context) => UsersPage()));
          } catch (e) {
            showAlert(context, 'Llene todas las casillas', 'porfavor');
          }
            }
          )
        ]
      );
    }
    // ButtonBlue(
    //     text: 'Confirmar', 
    //     onPressed: () async {
    //       FocusScope.of(context).unfocus();
    //       // print(jugador2Ctrl.text);
    //       // print(jugador3Ctrl.text);
    //       // print(jugador4Ctrl.text);

    //         Navigator.push(context, 
    //           MaterialPageRoute(builder: (context) => Round2(jugador2: widget.jugador2, jugador3: widget.jugador3, jugador4: widget.jugador4, totalJugadores: widget.totalJugadores,)));
    // )};
    return Container();
    
  }
}