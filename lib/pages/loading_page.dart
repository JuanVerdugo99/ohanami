import 'package:flutter/material.dart';
import 'package:ohanami/pages/login_page.dart';
import 'package:ohanami/pages/users_page.dart';
import 'package:ohanami/services/auth_service.dart';
import 'package:provider/provider.dart';

class LoadingPage extends StatelessWidget {
  const LoadingPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: checkLoginState(context),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) { 
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
                Text('Cargando')
              ],
            ),
          );
         }
      ),
    );
  }

  Future checkLoginState(BuildContext context) async {
    final authService = Provider.of<AuthService>(context, listen: false);
    final auth = await authService.isLoggedIn();

    if(auth) {
      Navigator.pushReplacement(
        context, 
        PageRouteBuilder(
          pageBuilder: (_, __, ___ ) => UsersPage(),
          transitionDuration: Duration(milliseconds: 0)
        )  
      );
    } else {
      Navigator.pushReplacement(
        context, 
        PageRouteBuilder(
          pageBuilder: (_, __, ___ ) => LoginPage(),
          transitionDuration: Duration(milliseconds: 0)
        )  
      );
    }
  }

}