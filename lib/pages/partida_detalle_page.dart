import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:ohanami/global/environment.dart';
import 'package:ohanami/pages/stats_page.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class PartidaDetallePage extends StatefulWidget {
  const PartidaDetallePage({
    Key? key,
    required this.userId,
  }) : super(key: key);

  final String userId;

  @override
  _PartidaDetallePageState createState() => _PartidaDetallePageState();
}

class _PartidaDetallePageState extends State<PartidaDetallePage> {
  late Map data;
  List partida = [];
  int counter = 0;
  int numeroMayor = 0;
  List<GDPData>? _chartData;
  
  getPartida() async {
    http.Response response = await http.get(Uri.parse('${Environment.apiUrl}/partida/una/${widget.userId}'));
    data = json.decode(response.body);
    setState(() {
      partida = data['partidas'];
      counter = partida[0]["jugadores"].length;
      for (var i = 0; i < counter; i++) {
        if( partida[0]["jugadores"][i]["puntuacion"] > numeroMayor) {
          numeroMayor = partida[0]["jugadores"][i]["puntuacion"];
        }
      }
      _chartData = getChartData();
    });
  }

  @override
  void initState() {
    getPartida();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: ConstrainedBox(
            constraints: BoxConstraints(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                padding: EdgeInsets.symmetric(horizontal: 50),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: Row(
                            children: [
                              IconButton(
                                icon: new Icon(Icons.arrow_back),
                                highlightColor: Colors.blue,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              )
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            ListView.builder(
                                shrinkWrap: true,
                                itemCount: counter,
                                itemBuilder: (BuildContext context, int index) {
                                  return Card(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        ListTile(
                                          // leading: Icon(Icons.videogame_asset_rounded),
                                          title: Text("${partida[0]["jugadores"][index]["jugador"]}"),
                                          subtitle: Text("Puntuación: ${partida[0]["jugadores"][index]["puntuacion"]}"),
                                          trailing: numeroMayor == partida[0]["jugadores"][index]["puntuacion"] ? Icon(Icons.assistant_photo) : null,
                                          onTap: () {
                                          //  Navigator.push(context, 
                                          //                 MaterialPageRoute(builder: (context) => PartidaDetallePage()));
                                          },
                                        ),
                                      ]
                                    ) 
                                  );
                                },
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 30),
                              child: SfCircularChart(
                              title: ChartTitle(text: 'Tus Estadísticas'),
                              legend: Legend(isVisible: true, overflowMode: LegendItemOverflowMode.wrap),
                              palette: [
                                Colors.lightBlue,
                                Colors.green,
                                Colors.grey,
                                Colors.pinkAccent
                              ],
                              series: <CircularSeries>[ 
                                DoughnutSeries<GDPData, String>(
                                  dataSource: _chartData,
                                  xValueMapper: (GDPData data, _) => data.continent,
                                  yValueMapper: (GDPData data, _) => data.gdp,
                                  dataLabelSettings: DataLabelSettings(isVisible: true)
                                )],),
                            )
                          ],
                        )
                      ],
                    ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<GDPData> getChartData() {
    final List<GDPData> chartData = [
      GDPData(gdp: partida[0]['totalCartas']['cartasAzules'], continent: 'Total Azules'),
      GDPData(gdp: partida[0]['totalCartas']['cartasVerdes'], continent: 'Total Verdes'),
      GDPData(gdp: partida[0]['totalCartas']['cartasGrises'], continent: 'Total Grises'),
      GDPData(gdp: partida[0]['totalCartas']['cartasRosas'], continent: 'Total Rosas'),
    ];
    return chartData;
  }
}

class GDPData {
  final int gdp;
  final String continent;
  GDPData({
    required this.gdp,
    required this.continent,
  });
}

class Task {
  final int puntuacion;
  final String jugador;
  final String colorVal;
  final String ronda;
  Task({
    required this.puntuacion,
    required this.jugador,
    required this.colorVal,
    required this.ronda,
  });
  
}

// class scoreJugadores extends StatefulWidget {
//   const scoreJugadores({
//     Key? key,
//     required this.partida,
//   }) : super(key: key);
//   final List partida;

//   @override
//   _scoreJugadoresState createState() => _scoreJugadoresState();
// }

// class _scoreJugadoresState extends State<scoreJugadores> {

//   @override
//   Widget build(BuildContext context) {
//     // var jugadores = widget.partida.jugadores;
//     return ListView.builder(
//           shrinkWrap: true,
//           itemCount: 1,
//           itemBuilder: (BuildContext context, int index) {
//             return Card(
//               child: Column(
//                 mainAxisSize: MainAxisSize.min,
//                 children: [
//                   ListTile(
//                     // leading: Icon(Icons.videogame_asset_rounded),
//                     title: Text("Hola"),
//                     subtitle: Text("Puntuación: "),
//                     trailing: Icon(Icons.exit_to_app_outlined),
//                     onTap: () {
//                     //  Navigator.push(context, 
//                     //                 MaterialPageRoute(builder: (context) => PartidaDetallePage()));
//                     },
//                   ),
//                 ]
//               ) 
//             );
//           },
//       );
//   }
// }