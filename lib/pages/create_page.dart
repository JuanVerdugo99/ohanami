import 'package:flutter/material.dart';
import 'package:ohanami/helpers/show_alert.dart';
import 'package:ohanami/pages/round1_page.dart';
import 'package:ohanami/widgets/button_blue.dart';
import 'package:ohanami/widgets/custom_input.dart';

class CreatePage extends StatefulWidget {
  CreatePage({Key? key}) : super(key: key);

  @override
  _CreatePageState createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage> {

  final jugador2Ctrl = TextEditingController();
  final jugador3Ctrl = TextEditingController();
  final jugador4Ctrl = TextEditingController();

  @override
  void initState() {
    super.initState();
    jugador2Ctrl.text = '';
    jugador3Ctrl.text = '';
    jugador4Ctrl.text = '';
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 60),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            // Padding(
            //   padding: const EdgeInsets.only(top: 20),
            //   child: Row(
            //     children: [
            //       Icon(Icons.arrow_back),
            //       Text('')
            //     ],
            //   ),
            // ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(22.0),
                  child: Text("Nombre de los jugadores", style: TextStyle(fontSize: 20),),
                ),
                CustomInput(
                  icon: Icons.account_circle,
                  placeholder: 'Jugador 2',
                  keyboardType: TextInputType.text,
                  textController: jugador2Ctrl,
                  isPassword: false,
                ),
                CustomInput(
                  icon: Icons.account_circle,
                  placeholder: 'Jugador 3 (opcional)',
                  keyboardType: TextInputType.text,
                  textController: jugador3Ctrl,
                  isPassword: false,
                ),
                CustomInput(
                  // icon: Icons.copy_outlined,
                  icon: Icons.account_circle,
                  placeholder: 'Jugador 4 (opcional)',
                  keyboardType: TextInputType.text,
                  textController: jugador4Ctrl,
                  isPassword: false,
                ),
                 ButtonBlue(
                  text: 'Crear Partida', 
                  onPressed: () async {
                    FocusScope.of(context).unfocus();

                    if(jugador2Ctrl.text == '') {
                      showAlert(context, 'Oops, no puedes hacer eso', 'El jugador 2 es necesario');
                    } else if(jugador3Ctrl.text == '' && jugador4Ctrl.text != '') {
                      showAlert(context, 'Oops, no puedes hacer eso', 'Tienes que escribir el nombre del tercer jugador también');
                    } else if (jugador3Ctrl.text == '' && jugador4Ctrl.text == '') {
                      Navigator.push(context, 
                        MaterialPageRoute(builder: (context) => Round1(jugador2: jugador2Ctrl.text, jugador3: '', jugador4: '', totalJugadores: 2,)));
                    } else if (jugador4Ctrl.text == '') {
                      Navigator.push(context, 
                        MaterialPageRoute(builder: (context) => Round1(jugador2: jugador2Ctrl.text, jugador3: jugador3Ctrl.text, jugador4: '', totalJugadores: 3,)));
                    } else {
                      Navigator.push(context, 
                        MaterialPageRoute(builder: (context) => Round1(jugador2: jugador2Ctrl.text, jugador3: jugador3Ctrl.text, jugador4: jugador4Ctrl.text, totalJugadores: 4,)));
                    }


                    // final registerOk = await authService.register(nameCtrl.text.trim(), emailCtrl.text.trim(), passCtrl.text.trim());
    
                    // if(registerOk == true) {
                    //   Navigator.pushReplacementNamed(context, 'users');
                    // } else {
                    //   showAlert(context, 'Registro incorrecto', registerOk);
                    // }
                  }
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class PlayerPage extends StatefulWidget {
  PlayerPage({Key? key}) : super(key: key);

  @override
  _PlayerPageState createState() => _PlayerPageState();
}

class _PlayerPageState extends State<PlayerPage> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class CardsPage extends StatefulWidget {
  CardsPage({Key? key}) : super(key: key);

  @override
  _CardsPageState createState() => _CardsPageState();
}

class _CardsPageState extends State<CardsPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text("Cards"),
    );
  }
}