// To parse this JSON data, do
//
//     final userDb = userDbFromJson(jsonString);

import 'dart:convert';

UserDb userDbFromJson(String str) => UserDb.fromJson(json.decode(str));

String userDbToJson(UserDb data) => json.encode(data.toJson());

class UserDb {
    UserDb({
        required this.name,
        required this.email,
        required this.uid,
    });

    String name;
    String email;
    String uid;

    factory UserDb.fromJson(Map<String, dynamic> json) => UserDb(
        name: json["name"],
        email: json["email"],
        uid: json["uid"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "uid": uid,
    };
}
