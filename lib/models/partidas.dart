// To parse this JSON data, do
//
//     final partida = partidaFromJson(jsonString);

import 'dart:convert';

Partida partidaFromJson(String str) => Partida.fromJson(json.decode(str));

String partidaToJson(Partida data) => json.encode(data.toJson());

class Partida {
    Partida({
        required this.idUser,
        required this.jugadores,
        required this.totalCartas,
    });

    String idUser;
    List<Jugadore> jugadores;
    TotalCartas totalCartas;

    factory Partida.fromJson(Map<String, dynamic> json) => Partida(
        idUser: json["idUser"],
        jugadores: List<Jugadore>.from(json["jugadores"].map((x) => Jugadore.fromJson(x))),
        totalCartas: TotalCartas.fromJson(json["totalCartas"]),
    );

    Map<String, dynamic> toJson() => {
        "idUser": idUser,
        "jugadores": List<dynamic>.from(jugadores.map((x) => x.toJson())),
        "totalCartas": totalCartas.toJson(),
    };
}

class Jugadore {
    Jugadore({
        required this.jugador,
        required this.puntuacion,
        required this.id,
    });

    String jugador;
    int puntuacion;
    String id;

    factory Jugadore.fromJson(Map<String, dynamic> json) => Jugadore(
        jugador: json["jugador"],
        puntuacion: json["puntuacion"],
        id: json["_id"],
    );

    Map<String, dynamic> toJson() => {
        "jugador": jugador,
        "puntuacion": puntuacion,
        "_id": id,
    };
}

class TotalCartas {
    TotalCartas({
        required this.cartasAzules,
        required this.cartasVerdes,
        required this.cartasGrises,
        required this.cartasRosas,
    });

    int cartasAzules;
    int cartasVerdes;
    int cartasGrises;
    int cartasRosas;

    factory TotalCartas.fromJson(Map<String, dynamic> json) => TotalCartas(
        cartasAzules: json["cartasAzules"],
        cartasVerdes: json["cartasVerdes"],
        cartasGrises: json["cartasGrises"],
        cartasRosas: json["cartasRosas"],
    );

    Map<String, dynamic> toJson() => {
        "cartasAzules": cartasAzules,
        "cartasVerdes": cartasVerdes,
        "cartasGrises": cartasGrises,
        "cartasRosas": cartasRosas,
    };
}
