import 'dart:io';
import 'package:flutter/foundation.dart';

class Environment {
  //  static String apiUrl = Platform.isAndroid ? 'http://10.0.2.2:3000/api' : 'http://localhost:3000/api';
   static String apiUrl = defaultTargetPlatform == TargetPlatform.android ? 'http://10.0.2.2:3000/api' : 'http://localhost:3000/api';

}
