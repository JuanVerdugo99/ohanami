import 'package:flutter/material.dart';
import 'package:ohanami/routes/routes.dart';
import 'package:ohanami/services/auth_service.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthService(),)
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Ohanami',
        initialRoute: 'loading',
        routes: appRoutes
      ),
    );
  }
}