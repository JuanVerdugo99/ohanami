import 'package:flutter/material.dart';
import 'package:ohanami/pages/loading_page.dart';
import 'package:ohanami/pages/login_page.dart';
import 'package:ohanami/pages/register_page.dart';
import 'package:ohanami/pages/stats_page.dart';
import 'package:ohanami/pages/users_page.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'loading' : (_) => LoadingPage(),
  'login'   : (_) => LoginPage(),
  'register': (_) => RegisterPage(),
  'users'   : (_) => UsersPage(),
  'stats'   : (_) => StatsPage()
};