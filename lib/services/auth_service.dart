import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:ohanami/global/environment.dart';
import 'package:ohanami/models/login_response.dart';
import 'package:ohanami/models/partidas.dart';
import 'package:ohanami/models/users.dart';

class AuthService with ChangeNotifier {

  late UserDb user;
  bool _auth = false;

  final _storage = new FlutterSecureStorage();

  bool get auth => this._auth;
  set auth(bool value) {
    this._auth = value;
    notifyListeners();
  }

  static Future<String> getToken() async {
    final _storage = new FlutterSecureStorage();
    final token = await _storage.read(key: 'token');
    return token!;
  }

  static Future<void> deleteToken() async {
    final _storage = new FlutterSecureStorage();
    await _storage.delete(key: 'token');
  }

  Future<bool> login( String email, String password) async {

    this.auth = true;

    final data = {
      'email': email,
      'password': password
    };

    final res = await http.post(
      Uri.parse('${Environment.apiUrl}/login'), 
      body: jsonEncode(data),
      headers: {
        'Content-Type': 'application/json'
      }
    );

    this.auth = false;

    if( res.statusCode == 200) {
      final loginResponse = loginResponseFromJson(res.body);
      this.user = loginResponse.userDb;

      await this._saveToken(loginResponse.token);
      
      return true;

    } else {
      return false;
    }
    
  }

  Future register(String name, String email, String password) async {
    this.auth = true;

    final data = {
      'name': name,
      'email': email,
      'password': password
    };

    final res = await http.post(
      Uri.parse('${Environment.apiUrl}/login/new'), 
      body: jsonEncode(data),
      headers: {'Content-Type': 'application/json'}
    );

    this.auth = false;

    if( res.statusCode == 200) {
      final loginResponse =  loginResponseFromJson(res.body);
      this.user = loginResponse.userDb;

      await this._saveToken(loginResponse.token);
      
      return true;

    } else {
      final resBody = jsonDecode(res.body);
      return resBody['msg'];
    }
  }

  Future<bool> isLoggedIn() async {
    final token = await this._storage.read(key: 'token') ?? '';
    
    final res = await http.get(
        Uri.parse('${Environment.apiUrl}/login/renew'),
        headers: {
          'Content-Type': 'application/json',
          'x-token': token
        }
      );

      if( res.statusCode == 200) {
        final loginResponse =  loginResponseFromJson(res.body);
        user = loginResponse.userDb;

        await _saveToken(loginResponse.token);
        
        return true;

      } else {
        logout();
        return false;
      }
  }

  Future _saveToken(String token) async {
    return await _storage.write(key: 'token', value: token);
  }

  Future logout() async {
    await _storage.delete(key: 'token');
  }

// Future crearPartida(String jugador1, 
//                     String jugador2,
//                     int totalJugador1,
//                     int totalJugador2,
//                     [
//                       String jugador3 = '',
//                       String jugador4 = '',
//                       int totalJugador3 = 0,
//                       int totalJugador4 = 0,
//                     ]) async {

Future crearPartida(String userId, String jugador1, String jugador2, int puntuacion1, int puntuacion2,
                    double cartasAzules, double cartasVerdes, double cartasGrises, int cartasRosas,
                    [String jugador3 = '', int puntuacion3 = 0, String jugador4 = '', int puntuacion4 = 0]) async {
    // Map data = {
    //   'jugadores': {
    //     'jugador1': partida.jugadores.jugador1,
    //     'jugador2': partida.jugadores.jugador2,
    //     'jugador3': partida.jugadores.jugador3,
    //     'jugador4': partida.jugadores.jugador4,
    //   },
    //   'puntuaciones': {
    //     'totalJugador1': partida.puntuaciones.totalJugador1,
    //   'totalJugador2': partida.puntuaciones.totalJugador2,
    //   'totalJugador3': partida.puntuaciones.totalJugador3,
    //   'totalJugador4': partida.puntuaciones.totalJugador4
    //   }
    // };

    Map data = {};

    if(jugador3 == '') {
      data = {
        'idUser': userId,
        'jugadores': [
          {
            'jugador': jugador1,
            'puntuacion': puntuacion1
          },
          {
            'jugador': jugador2,
            'puntuacion': puntuacion2
          }
        ],
        'totalCartas': {
            'cartasAzules': cartasAzules,
            'cartasVerdes': cartasVerdes,
            'cartasGrises': cartasGrises,
            'cartasRosas': cartasRosas,
        }
      };
    } else if(jugador4 == ''){
      data = {
        'idUser': userId,
        'jugadores': [
          {
            'jugador': jugador1,
            'puntuacion': puntuacion1
          },
          {
            'jugador': jugador2,
            'puntuacion': puntuacion2
          },
          {
            'jugador': jugador3,
            'puntuacion': puntuacion3
          }
        ],
        'totalCartas': {
            'cartasAzules': cartasAzules,
            'cartasVerdes': cartasVerdes,
            'cartasGrises': cartasGrises,
            'cartasRosas': cartasRosas,
        }
      };
    } else {
       data = {
         'idUser': userId,
        'jugadores': [
          {
            'jugador': jugador1,
            'puntuacion': puntuacion1
          },
          {
            'jugador': jugador2,
            'puntuacion': puntuacion2
          },
          {
            'jugador': jugador3,
            'puntuacion': puntuacion3
          } ,
          {
            'jugador': jugador4,
            'puntuacion': puntuacion4
          }
        ],
        'totalCartas': {
            'cartasAzules': cartasAzules,
            'cartasVerdes': cartasVerdes,
            'cartasGrises': cartasGrises,
            'cartasRosas': cartasRosas,
        }
      };
    }

    

    final res = await http.post(
      Uri.parse('${Environment.apiUrl}/partida/create'), 
      body: jsonEncode(data),
      headers: {'Content-Type': 'application/json'}
    );
  }

  Future borrarPartida(String id) async {
    final res = await http.delete(
      Uri.parse('${Environment.apiUrl}/partida/${id}'));
  }

  Future obtenerPartidaElegida(String id) async {
    final res = await http.get(
      Uri.parse('${Environment.apiUrl}/partida/una/${id}'));
  }
    
  }




